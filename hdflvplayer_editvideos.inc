<?php
/**
 * @file
// $Id: hdflvplayer_profiles.inc,v 1.0 Created by Vasanth at 2010/01/13 11:10:29 Exp $
/*
 * This file is used to edit the video details.
 */
/* * ****************************************************** HDFLV VIDEO FORM ************************************************************************ */
/*
 * Edit videos from elements.
 */
function hdflvplayer_video_edit_form(&$form_state, $pid, $id=NULL) {

    if ($pid != '') {
        $result = db_fetch_array(db_query('SELECT * FROM {hdflvplayerupload} WHERE id = "%s"', $pid));
    }

    /* Call css and js. */

    drupal_add_js(drupal_get_path('module', 'hdflvplayer') . '/js/ajaxupload.3.5.js', 'external', 'footer');
    drupal_add_js(drupal_get_path('module', 'hdflvplayer') . '/js/jquery-1.3.2.js', 'external', 'footer');
    drupal_add_js(drupal_get_path('module', 'hdflvplayer') . '/js/upload_script.js', 'external', 'footer');
    drupal_add_js(drupal_get_path('module', 'hdflvplayer') . '/js/videoadd.js', 'external', 'footer');

    drupal_add_css(drupal_get_path('module', 'hdflvplayer') . '/style/hdflvplayeradd.css');

    /* Call playlist function to retrive the playlists. */

    $play_list = hdflvplayer_playlist_get_configs();

    $play_list[0] = "None";

    /* playlist Select box. */

    foreach ($play_list as $key => $play) {
        if ($key == $result['playlistid']) {
            $play_key = "value = $key selected=selected";
        }
        else {
            $play_key = "value = $key";
        }

        $play_listval .= '<option ' . $play_key . ' >' . $play . '</option>';
    }

    /*
     * Define form as array because it contain the form values and passed to submit function.
     */
    $form = array();

    $youtube_display = "";

    $upload_display = "";

    $url_display = "";

    $ad_roll = hdflvplayer_ad_roll();

    if ($ad_roll != '') {

        foreach ($ad_roll as $key => $ads) {
            if ($key == $result['prerollid']) {
                $pre_key = "value = $key selected=selected";
            }
            else {
                $pre_key = "value = $key";
            }
            if ($key == $result['postrollid']) {
                $post_key = "value='.$key.' selected=selected";
            }
            else {
                $post_key = "value='.$key.'";
            }
            $pre_ads .= '<option ' . $pre_key . ' >' . $ads . '</option>';
            $post_ads .= '<option ' . $post_key . ' >' . $ads . '</option>';
        }
    }

    /* Define radio button value. */

    $radio_button = $result['filepath'];

    /* If filepath upload. check upload button and make upload display-block. */

    if ($result['filepath'] == 'upload') {

        $youtube_display = "display:none;";

        $upload_display = "display:block;";

        $url_display = "display:none;";

        $upload = 'checked';
    }

    /* If filepath youtube. check youtube button and make youtube display-block. */
    elseif ($result['filepath'] == 'youtube') {

        $youtube_display = "display:block;";

        $upload_display = "display:none;";

        $url_display = "display:none;";

        $youtube = 'checked';
    }

    /* If filepath url. check url button and make url display-block. */ 
    elseif ($result['filepath'] == 'url') {

        $youtube_display = "display:none;";

        $upload_display = "display:none;";

        $url_display = "display: block;";

        $url = 'checked';
    }

    /* If filepath url. check url button and make url display-block. */
    elseif ($result['filepath'] == 'ffmpeg') {

        $youtube_display = "display:none;";

        $upload_display = "display:none;";

        $url_display = "display: none;";

        $ffmpeg = 'checked';
    }
    /* If streameroption none. check none button. */

    if ($result['streameroption'] == 'none') {
        $none = 'checked';
    }

    /* If streameroption lighttpd. check lighttpd button. */
    elseif ($result['streameroption'] == 'lighttpd') {
        $lighttpd = 'checked';
    }

    /* If streameroption rtmp. check rtmp button. */
    elseif ($result['streameroption'] == 'rtmp') {
        $rtmp = 'checked';
    }
    if ($result['postrollads'] == '1') {
        $radio_button = 'checked';
    }
    else {
        $disable_post = 'checked';
    }
    if ($result['prerollads'] == '1') {
        $enable_pre = 'checked';
    }
    else {
        $disable_pre = 'checked';
    }
    if ($result['islive'] == '1') {
        $islive_yes = 'checked';
    } 
    else {
        $islive_no = 'checked';
    }

    /*     * ********************************************************** Form Fields******************************************************************* */
    $form['#prefix'] =
            '<div style="padding:10px 0;border-top: 1px solid #CCC;margin-top:10px;">
                <span class="title">Streamer option :</span>
                  <input type="radio" name="streameroption" id="stremerOpt1" ' . $none . ' value="none" onclick="streamerl(this.value)" /><lable>None</lable>
                  <input type="radio" name="streameroption" id="stremerOpt2" value="lighttpd" ' . $lighttpd . ' onClick="streamerl(this.value)" /> <lable>lighttpd</lable>
                  <input type="radio" name="streameroption" id="stremerOpt3" value="rtmp" ' . $rtmp . ' onClick="streamerl(this.value)" /> <lable>rtmp</lable>
             </div>

    <div id="file_path">
        <span class="title">File Path :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span>
            <input type="radio" name="agree" id="upload" value="upload" ' . $upload . ' onClick="t1(this.value)" /><lable> Upload file</lable>
            <input type="radio" name="agree" id="youtube" value = "youtube" ' . $youtube . ' onClick="t1(this.value)" /> <lable>YouTube URL</lable>
            <input type="radio" name="agree" id="url" value = "url" ' . $url . ' onClick="t1(this.value)" /> <lable>Custom URL</lable>
            <input type="radio" name="agree" id="ffmpeg" value= "ffmpeg" ' . $ffmpeg . ' onclick="t1(this.value);"/><lable>FFmpeg</lable>
    </div>

<div id="ffmpeg_path" style="disable:none;">
    <table>
    <tr><th scope="row">Upload Video</th>
         <td>
            <div id="ffmpeg_video" ><span>Upload Video</span></div><div id ="ffmpeg_message_url"><span id="ffmpeg_status" ></span>
		<ul id="ffmpeg_files" ></ul>
            </div>
          </td>
   </tr>
   </table>
</div>


<div id="upload_path" style="' . $upload_display . '">
<table>
     <tr> <th scope="row">Upload Video</th>
                <td width="20%">
                    <div id="upload_video" ><span>Upload Video</span></div><div id ="video_message_url"><span id="video_status" ></span>
		<ul id="video_files" ></ul></div>
 </td><td><div id="retrive_video" class="retrivevalues">' . $result['videourl'] . '</div></td></tr>
            <tr>
            <th scope="row">Upload HD Video(optional)</th>
                <td>
                   <div id="hd_video" ><span>Upload HD Video</span></div><div id ="hd_message_url"><span id="hd_status" ></span>
		<ul id="hd_files" ></ul></div>
            </td><td><div id="retrive_hd" class="retrivevalues">' . $result['hdurl'] . '</div></td></tr>
            <tr><th scope="row">Upload Thumb Image</th><td>

                   <div id="thumb" ><span>Upload Thumb Image</span></div><div id ="thumb_message_url"><span id="thumb_status" ></span>
		<ul id="thumb_files" ></ul></div>
            </td><td><div id="retrive_thumb" class="retrivevalues">' . $result['thumburl'] . '</div></td></tr>
    </table>
    </div>


<div id="youtube_url" class="inside" style="' . $youtube_display . '">
    <table class="form-table">
        <tr id="youtube_tr">
            <th scope="row">Youtube Link</th>
            <td><input type="text" size="50" name="youtube_value" id="youtube_value" onchange="youtube(this.value)" value="' . $result['videourl'] . '" >
                <br /><p>Here you need to enter the URL to the video file
            <br />It accept also a Youtube link:
http://www.youtube.com/watch?v=tTGHCRUdlBs</p></td>
        </tr>
    </table>
</div>

<div id="stream_url" style="display:none">
    <table>
        <tr id="stream_path" name="stream_path" ><td><span class="title">Streamer Path:</span></td>
            <td>
                <input type="text" name="streamname"  id="streamname" style="width:300px" maxlength="250" value=' . $result['streamerpath'] . '>
            </td>
        </tr>
        <tr id="islive_visible" name="islive_visible">
            <td><span class="title">Is Live:</span></td>
            <td>
                <input type="radio" name="islive[]"  id="islive1"   value="0" ' . $islive_no . '>No
                <input type="radio" name="islive[]"  id="islive2"  value="1" ' . $islive_yes . '>Yes
            </td>
        </tr>
    </table>
</div>

 <div id="customurl" class="inside" style="' . $url_display . '">
                <table class="form-table" style="margin:0px;">
                    <tr>
                        <th scope="row">URL to video file</th>
                        <td><input type="text" size="50" name="url_video" id="url_video" value="' . $result['videourl'] . '"/>
                            <br /><p>Here you need to enter the URL to the video file</p>
                        </td></tr>
                        <tr><th scope="row">URL to HD video file(optional)</th>
                        <td><input type="text" size="50" name="url_hd" id="url_hd" value="' . $result['hdurl'] . '"/>
                            <br /><p>Here you need to enter the URL to the HD video file</p>
                        </td>
                    </tr>
                     <tr><th scope="row">URL to Thumb image file</th>
                        <td><input type="text" size="50" name="url_image" id="url_image" value="' . $result['thumburl'] . '"/>
                            <br /><p>Here you need to enter the URL to the image file</p>
                        </td>
                    </tr>
                    <tr><th scope="row">URL to Thumb Preview image file(Optional)</th>
                        <td><input type="text" size="50" name="url_previmage" id="url_previmage" value="' . $result['previewurl'] . '"/>
                            <br /><p>Here you need to enter the URL to the Preview image file</p>
                        </td>
                    </tr>
                </table>
            </div>


<div id="ads">
<div id="roll">
    <span class="title" >Preroll:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </span>
    <input type="radio" name="preroll" id="preroll1"  value="1" ' . $enable_pre . ' onclick="roll(this.value)" /><lable>Enable</lable>
    <input type="radio" name="preroll" id="preroll12" ' . $disable_pre . ' value="0" onClick="roll(this.value)" /> <lable>Disable</lable>
</div>

<div id="roll_select" style="display:none">
     <tr>
        <span class="title" >Preroll Ads:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <td><select id="preroll_id" onChange="selectpre(this.value)"><option value="">Select</option>' . $pre_ads . '</select>
          </td>
    </tr>
</div>


<div>
    <span class="title">Postroll:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    <input type="radio" name="postroll" id="postroll1"  value="1" ' . $radio_button . ' onclick="postroll(this.value)" /><lable>Enable</lable>
    <input type="radio" name="postroll" id="postroll2" ' . $disable_post . ' value="0" onClick="postroll(this.value)" /> <lable>Disable</lable>
</div>

<div id="postroll_select" style="display:none">
   <tr>
      <span class="title" >Postroll Ads:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <td><select id="postroll_id" onChange="selectpost(this.value)"><option value="">Select</option>' . $post_ads . '</select>
          </td>
    </tr>
 </div>

</div>
<div id="playlist"> <span class="title" >Select Playlist:&nbsp;&nbsp;&nbsp;&nbsp;</span>
<select id="playlist_id" name="playlist_id" onChange="selectplaylist(this.value)">' . $play_listval . '</select></div>
';

    $form['#attributes'] = array('name' => 'uploadfiles', 'onsubmit' => 'return chkbut()');

    /* set the hidden values to store the database. */

    $form['hiddenfile'] = array(
        '#prefix' => '<input type="hidden" name="normalvideoform-value" id="normalvideoform-value" value=""  />
                     <input type="hidden" name="hdvideoform-value" id="hdvideoform-value" value="" />
                     <input type="hidden" name="thumbimageform-value" id="thumbimageform-value" value="" />
                     <input type="hidden" name="previewimageform-value" id="previewimageform-value" value="" />
                     <input type="hidden" name="ffmpegform-value" id="ffmpegform-value" value="" />
                    <input type="hidden" name="youtube-value" id="youtube-value"  value="" />
                    <input type="hidden" name="customurl" id="custom_url"  value="" />
                    <input type="hidden" name="customhd" id="custom_hd"  value="" />
                    <input type="hidden" name="customimage" id="custom_image"  value="" />
                    <input type="hidden" name="custompreimage" id="custom_preimage"  value="" />
                    <input type="hidden" name="radiobutton" id="radiobutton"  value=' . $radio_button . '/>
                    <input type="hidden" name="stream" id="stream"  value="none" />
                    <input type="hidden" name="preroll" id="preroll"  value="" />
                    <input type="hidden" name="postroll" id="postroll"  value="" />
                    <input type="hidden" name="prerollid" id="prerollid"  value="" />
                    <input type="hidden" name="postrollid" id="postrollid"  value="" />
                    <input type="hidden" name="playlistid" id="playlistid"  value="" />
                    <input type="hidden" name="fileoption" id="fileoption"  value="none" />',
        '#type' => 'hidden',
        '#name' => 'id',
        '#id' => 'idvalue',
        '#value' => $pid,
    );


    $form['title'] = array(
        '#title' => t('Title'),
        '#type' => 'textfield',
        '#name' => 'title',
        '#id' => 'title',
        '#required' => TRUE,
        '#value' => $result['title'],
    );

    $form['description'] = array(
        '#title' => t('Description'),
        '#type' => 'textarea',
        '#name' => 'description',
        '#id' => 'description',
        '#value' => $result['description'],
    );

    $form['targeturl'] = array(
        '#title' => t('Target Url'),
        '#type' => 'textfield',
        '#name' => 'targeturl',
        '#id' => 'targeturl',
        '#value' => $result['targeturl'],
    );


    $form['order'] = array(
        '#title' => t('Order'),
        '#type' => 'textfield',
        '#name' => 'order',
        '#id' => 'order',
        '#size' => '10',
        '#value' => $result['ordering'],
    );

    $form['download'] = array(
        '#type' => 'radios',
        '#title' => t('Download'),
        '#default_value' => $result['download'],
        '#options' => array('1' => t('Enable'), '0' => t('Disable')),
    );

    $form['published'] = array(
        '#type' => 'radios',
        '#title' => t('Published'),
        '#default_value' => $result['published'],
        '#options' => array('1' => t('Enable'), '0' => t('Disable')),
        '#required' => TRUE,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );

    return $form;
}

/* * ************************************ handles the submit of an individual profile form********************************************************** */
/*
 * Edit videos from elements submit function.
 */

function hdflvplayer_video_edit_form_submit($form, &$form_state) {

    /* get the values from post. */

    $playlist_id = $form_state['clicked_button']['#post']['playlistid'];

    $published = $form_state['clicked_button']['#post']['published'];

    $title = addslashes($form_state['clicked_button']['#post']['title']);

    $videos = $form_state['clicked_button']['#post']['videos'];

    $ffmpeg = $form_state['clicked_button']['#post']['ffmpeg'];

    $ffmpeg_videos = $form_state['clicked_button']['#post']['ffmpeg_videos'];

    $ffmpeg_thumbimages = $form_state['clicked_button']['#post']['ffmpeg_thumbimages'];

    $ffmpeg_previewimages = $form_state['clicked_button']['#post']['ffmpeg_previewimages'];

    $ffmpeg_hd = $form_state['clicked_button']['#post']['ffmpeg_hd'];

    $duration = $form_state['clicked_button']['#post']['duration'];

    $download = $form_state['clicked_button']['#post']['download'];

    $postroll_ads = $form_state['clicked_button']['#post']['postroll'];

    $preroll_ads = $form_state['clicked_button']['#post']['preroll'];

    $postroll_id = $form_state['clicked_button']['#post']['postrollid'];

    $preroll_id = $form_state['clicked_button']['#post']['prerollid'];

    $target_url = $form_state['clicked_button']['#post']['targeturl'];

    $description = addslashes($form_state['clicked_button']['#post']['description']);

    $ordering = $form_state['clicked_button']['#post']['order'];

    $streamer_option = $form_state['clicked_button']['#post']['stream'];

    $id = $form_state['clicked_button']['#post']['id'];

    /*
     * If radiobutton value is youtube make thumnail and preview image.
     */
    if ($form_state['clicked_button']['#post']['radiobutton'] == 'youtube') {

        $ytb_pattern = "@youtube.com\/watch\?v=([0-9a-zA-Z_-]*)@i";
        
        $file_path = 'youtube';

        if (preg_match($ytb_pattern, stripslashes($_POST['youtube-value']), $match)) {

            /* Get youtube details. */

            $youtube_data = hd_GetSingleYoutubeVideo($match[1]);

            if ($youtube_data) {
                $thumb_url = "thumburl= '" . $youtube_data['thumbnail_url'] . "', previewurl= '" . $youtube_data['thumbnail_url'] . "',";
            }

            $video_url = "videourl= '" . $form_state['clicked_button']['#post']['youtube-value'] . "',";
            
            $hd_url = '';
        }
        else {
            drupal_set_message(t('Please enter a A Valid YouTube URL!'));
        }
    }

    /* If radiobutton value is url. */

    if ($form_state['clicked_button']['#post']['radiobutton'] == 'url') {

        if ($form_state['clicked_button']['#post']['customurl'] == '') {
            drupal_set_message(t('Enter A Valid URL!'));
        }
        else {

            $video_path = $form_state['clicked_button']['#post']['customurl'];

            if ($video_path != '') {
                $video_url = "videourl= '" . $video_path . "',";
            }

            $hd_path = $form_state['clicked_button']['#post']['customhd'];

            if ($hd_path != '') {
                $hd_url = "hdurl= '" . $hd_path . "',";
            }

            $thumb_path = $form_state['clicked_button']['#post']['customimage'];

            if ($thumb_path != '') {

                $thumb_path = $form_state['clicked_button']['#post']['custompreimage'];

                if ($thumb_path != '') {

                    $preview_url = $thumb_path;
                }

                $thumb_url = "thumburl= '" . $thumb_path . "', previewurl= '" . $preview_url . "',";
            }

            $file_path = 'url';
        }
    }

    /* If radiobutton value is ffmpeg. */

    if ($_POST['radiobutton'] == 'ffmpeg') {

        $video = 'http://localhost/drupal-6.20/sites/default/files/hdflvplayer/hdvideos/';

        $file_name = $_POST['ffmpegform-value'];

        $ffmpeg_path = 'D:\xampp\htdocs\ffmpeg\ffmpeg.exe';

        $file = explode('.', $file_name);

        $img_path = 'D:\xampp\htdocs\ffmpeg';

        $dest_file = $video . $file_name;

        $jpg_resolution = "320x240";

        $target_path = $file[0] . ".jpeg";

        /*
         *  Convert any type of videos in to flv and get thumb image from frames of video.
         *  Using ffmpeg.
         */

        exec($ffmpeg_path . ' -i ' . $dest_file . ' -ab 56 -ar 44100 -b 200 -r 15 -s 320x240 -f flv ' . $img_path . '\finale.flv');

        exec($ffmpeg_path . ' -i ' . $dest_file . ' -an -ss 3 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg ' . $img_path . '\thumbimage.jpg');

        $ffmpeg_videos = $file[0] . ".flv";

        $ffmpeg_hd = $_POST['ffmpegform-value'];

        $ffmpeg_thumbimages = $file[0] . ".jpg";

        $ffmpeg_previewimages = $ffmpeg_thumbimages;

        $file_path = 'ffmpeg';
    }

    /* If radiobutton value is upload. */

    if ($form_state['clicked_button']['#post']['radiobutton'] == 'upload') {

        $video_path = $form_state['clicked_button']['#post']['normalvideoform-value'];

        if ($video_path != '') {
            $video_url = "videourl= '" . $video_path . "',";
        }

        $hd_path = $form_state['clicked_button']['#post']['hdvideoform-value'];

        if ($hd_path != '') {
            $hd_url = "hdurl= '" . $hd_path . "',";
        }

        $thumb_path = $form_state['clicked_button']['#post']['thumbimageform-value'];

        if ($thumb_path != '') {
            $thumb_url = "thumburl= '" . $thumb_path . "', previewurl= '" . $thumb_path . "',";
        }

        $file_path = 'upload';
    }

    // parse the data out of the form
    /* If radiobutton value is rtmp. */

    if ($form_state['clicked_button']['#post']['stream'] == 'rtmp') {

        $streamer_path = $form_state['clicked_button']['#post']['streamerpath'];

        $is_live = $form_state['clicked_button']['#post']['islive'];
    }
    else {

        $streamer_path = '';

        $is_live = '';
    }

    /* database query for update the values into database. */

    db_query("update {hdflvplayerupload} SET published = '" . $published . "', title = '" . $title . "',
                  ffmpeg_videos = '" . $ffmpeg_videos . "', ffmpeg_thumbimages = '" . $ffmpeg_thumbimages . "', ffmpeg_previewimages = '" . $ffmpeg_previewimages . "',
                    ffmpeg_hd= '" . $ffmpeg_hd . "', filepath= '" . $file_path . "',$video_url $thumb_url $hd_url playlistid= '" . $playlist_id . "', duration= '" . $duration . "', ordering= '" . $ordering . "', streamerpath= '" . $streamer_path . "', streameroption= '" . $streamer_option . "', postrollads= '" . $postroll_ads . "', prerollads= '" . $preroll_ads . "',
                            description= '" . $description . "', targeturl= '" . $target_url . "', download= '" . $download . "', prerollid= '" . $preroll_id . "', postrollid= '" . $postroll_id . "',  islive = '" . $is_live . "'where id= '" . $id . "'");

    /* Set message after update the values into database. */

    drupal_set_message(t('Updated into Database'));

    /* Set Redirect path after update the values into database. */

    $form_state['redirect'] = 'admin/hdflvplayer/videos/list';
}

/* * ********************************************************** Return Youtube single video******************************************************** */

function hd_GetSingleYoutubeVideo($youtube_media) {

    if ($youtube_media == '')
        return;

    $url = 'http://gdata.youtube.com/feeds/api/videos/' . $youtube_media;

    $ytb = hd_ParseYoutubeDetails(hd_GetYoutubePage($url));

    return $ytb[0];
}

/* * ********************************************************* Parse xml from Youtube****************************************************************** */

function hd_ParseYoutubeDetails($ytvideo_xml) {

    // Create parser, fill it with xml then delete it
    $yt_xml_parser = xml_parser_create();

    xml_parse_into_struct($yt_xml_parser, $ytvideo_xml, $yt_vals);

    xml_parser_free($yt_xml_parser);

    // Init individual entry array and list array

    $yt_video = array();

    $yt_vidlist = array();

    // is_entry tests if an entry is processing

    $is_entry = TRUE;

    // is_author tests if an author tag is processing

    $is_author = FALSE;

    foreach ($yt_vals as $yt_elem) {

        // If no entry is being processed and tag is not start of entry, skip tag
        if (!$is_entry && $yt_elem['tag'] != 'ENTRY')
            continue;

        // Processed tag
        switch ($yt_elem['tag']) {
            case 'ENTRY' :
                if ($yt_elem['type'] == 'open') {

                    $is_entry = TRUE;

                    $yt_video = array();
                }
                else {

                    $yt_vidlist[] = $yt_video;

                    $is_entry = FALSE;
                }
                break;
            case 'ID' :

                $yt_video['id'] = substr($yt_elem['value'], -11);

                $yt_video['link'] = $yt_elem['value'];

                break;

            case 'PUBLISHED' :

                $yt_video['published'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'UPDATED' :

                $yt_video['updated'] = substr($yt_elem['value'], 0, 10) . ' ' . substr($yt_elem['value'], 11, 8);

                break;
            case 'MEDIA:TITLE' :

                $yt_video['title'] = $yt_elem['value'];

                break;
            case 'MEDIA:KEYWORDS' :

                $yt_video['tags'] = $yt_elem['value'];

                break;
            case 'MEDIA:DESCRIPTION' :

                $yt_video['description'] = $yt_elem['value'];

                break;
            case 'MEDIA:CATEGORY' :

                $yt_video['category'] = $yt_elem['value'];

                break;
            case 'YT:DURATION' :

                $yt_video['duration'] = $yt_elem['attributes'];

                break;
            case 'MEDIA:THUMBNAIL' :
                if ($yt_elem['attributes']['HEIGHT'] == 240) {

                    $yt_video['thumbnail'] = $yt_elem['attributes'];

                    $yt_video['thumbnail_url'] = $yt_elem['attributes']['URL'];
                }
                break;
            case 'YT:STATISTICS' :

                $yt_video['viewed'] = $yt_elem['attributes']['VIEWCOUNT'];

                break;
            case 'GD:RATING' :

                $yt_video['rating'] = $yt_elem['attributes'];

                break;
            case 'AUTHOR' :

                $is_author = ($yt_elem['type'] == 'open');

                break;
            case 'NAME' :
                if ($is_author)
                    $yt_video['author_name'] = $yt_elem['value'];

                break;
            case 'URI' :
                if ($is_author)
                    $yt_video['author_uri'] = $yt_elem['value'];

                break;
            default :
        }
    }

    unset($yt_vals);

    return $yt_vidlist;
}

/* * ************************************************************ Returns content of a remote page************************************************************ */
/* Still need to do it without curl */

function hd_GetYoutubePage($url) {

    // Try to use curl first
    if (function_exists('curl_init')) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $xml = curl_exec($ch);

        curl_close($ch);
    }
    // If not found, try to use file_get_contents (requires php > 4.3.0 and allow_url_fopen)
    else {
        $xml = @file_get_contents($url);
    }

    return $xml;
}